﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    internal class Room
    {
        public int couch { get; private set; }
        public double weight_limit { get; private set; }
        List<Bed> infobed = new List<Bed>();
        //List<Patient> people = new List<Patient>();


        public Room(int couch, double weight_limit)
        {
            this.couch = couch;
            this.weight_limit = weight_limit;
            Bedcreate(couch);

        }

        public void Bedcreate(int couch)
        {
            for (int i=0; i< couch; i++)
            {
                infobed.Add(new Bed("L" + (i + 1), weight_limit));
            }
        }
        
        public void addPatient(Patient patient, int no)
        {
            if (check(patient, no))
            {
                infobed[no].occupation = true;
                infobed[no].victim = patient.surname +" "+ patient.lastname;
            }
            
        }
        public void removePatient(Patient patient, int no)
        {
            infobed[no].occupation = false;
            infobed[no].victim = null;
        }
        public bool check(Patient patient, int no)
        {
            if (infobed[no].occupation == false)
            { 
                if (patient.weight <= weight_limit)
                {
                    if (patient.isPatient)
                    {
                        Console.WriteLine("Is patient.");
                        return true;
                        
                    }
                }
            }
            return false;
        }

    }
}
