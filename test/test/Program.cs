﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Patient> Patients = new List<Patient>();
            List<Room> Rooms = new List<Room>();
            Patient patient1 = new Patient("Eren", "Yeager", 7707046908, 80, 160);
            Patient patient2 = new Patient("Levi", "Ackermann", 6409217672, 90, 150);
            Patient patient3 = new Patient("Armin", "Arlelt", 8102083418, 120, 185);
            Patient patient4 = new Patient("Mikasa", "Ackermann", 0104096762, 100, 174);


            Room area1 = new Room(3, 130);
            Room area2 = new Room(1, 200);
            Room area3 = new Room(5, 100);
            Rooms.Add(area1);
            Rooms.Add(area2);
            Rooms.Add(area3);

            area1.addPatient(patient3, 1);
            area2.addPatient(patient1, 0);
            area3.addPatient(patient2, 4);
            area1.addPatient(patient4, 2);
            Console.ReadKey();
        }
    }
}
