﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    internal class Bed
    {
        public string bedno { get; private set; }
        public double max_weight { get; private set; }
        public bool occupation { get; set; }
        public string victim { get; set; }
            
        
        public Bed(string bedno, double max_weight)
        {
            occupation = false;
            this.bedno = bedno;
            this.max_weight = max_weight;
            victim = null;

        }
    }
}
