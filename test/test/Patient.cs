﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    internal class Patient
    {
        public string surname { get; private set; }
        public string lastname { get; private set; }
        public long pin { get; private set; }
        public double weight { get; private set; }
        public double height { get; private set; }
        public double bmi { get; private set; }
        public bool isPatient { get; private set; }

        public Patient(string surname, string lastname, long pin, double weight, double height)
        {
            this.surname = surname;
            this.lastname = lastname;
            this.pin = pin;
            this.weight = weight;
            this.height = height;
        }
        public void weight_loss(double loss)
        {
            weight -= loss;
        }
        public void weight_gain(double gain)
        {
            weight += gain;
        }
        public void checkPatient()
        {
            double BMI = weight / (height / 100 * height / 100);
            if (BMI < 18.5 || BMI > 25)
            {
                isPatient = true;
            }
            else isPatient = false;
        }
        public void get_info()
        {
            double BMI = weight / (height/100 * height/100);
            Console.WriteLine("Patient: {0} {1}, personal identification number: {2}, weight: {3}kg , height: {4}cm , BMI: {5}", surname, lastname, pin, weight, height, BMI);
        }

    }
}
